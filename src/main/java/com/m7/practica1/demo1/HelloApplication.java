package com.m7.practica1.demo1;

import javafx.application.Application;
import javafx.beans.binding.NumberBinding;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Objects;


public class HelloApplication extends Application {
    public static void main(String[] args) {
        launch();
    }


    public static Scene scene;
    public static Scene sceneMarketing;
    public static Scene sceneDistribution;
    public static Scene sceneCost;
    public static Stage stageOriginal;

    @Override
    public void start(Stage stage) {

        stageOriginal = stage;


        stageOriginal.setTitle("Ventana Sales");
        stageOriginal.setWidth(800);
        stageOriginal.setHeight(600);

        //Original
        BorderPane border = new BorderPane();
        HBox hbox = addHBox();
        border.setTop(hbox);
        border.setLeft(addVBox());
        addStackPane(hbox);
        border.setCenter(addAnchorPane(addGridPane("Sales:", "Goods", "Services")));
        border.setRight(addFlowPaneSales());

        //Marketing
        BorderPane borderMarketing = new BorderPane();
        HBox hboxMarketing = addHBox();
        borderMarketing.setTop(hboxMarketing);
        borderMarketing.setLeft(addVBox());
        addStackPane(hboxMarketing);
        borderMarketing.setCenter(addAnchorPane(addGridPane("Marketing:","Internet","Local")));
        borderMarketing.setRight(addFlowPaneMarketing());
        sceneMarketing =new Scene(borderMarketing);

        //Distribution
        BorderPane borderDistribution = new BorderPane();
        HBox hboxDistribution = addHBox();
        borderDistribution.setTop(hboxDistribution);
        borderDistribution.setLeft(addVBox());
        addStackPane(hboxDistribution);
        borderDistribution.setCenter(addAnchorPane(addGridPane("Distribution:","Internat","External")));
        borderDistribution.setRight(addFlowPaneSales());
        sceneDistribution =new Scene(borderDistribution);

        //Costs
        BorderPane borderCost = new BorderPane();
        HBox hboxCost = addHBox();
        borderCost.setTop(hboxCost);
        borderCost.setLeft(addVBox());
        addStackPane(hboxCost);
        borderCost.setCenter(cost());
        borderCost.setRight(addFlowPaneSales());
        sceneCost =new Scene(borderCost);



        //Setting Original Stage and css's
        scene = new Scene(border);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("css/layoutstyles.css")).toExternalForm());
        sceneDistribution.getStylesheets().add(Objects.requireNonNull(getClass().getResource("css/layoutstyles.css")).toExternalForm());
        sceneMarketing.getStylesheets().add(Objects.requireNonNull(getClass().getResource("css/layoutstyles.css")).toExternalForm());
        sceneCost.getStylesheets().add(Objects.requireNonNull(getClass().getResource("css/layoutstyles.css")).toExternalForm());
        stageOriginal.setScene(scene);
        stageOriginal.show();
    }
    public VBox cost(){
        VBox vbox = new VBox();
        vbox.setId("cost");
        Slider sliderPies = new Slider(0, 100, 0);
        sliderPies.setShowTickMarks(true);
        sliderPies.setShowTickLabels(true);

        NumberBinding pulgadas = sliderPies.valueProperty().multiply(12);
        NumberBinding metros = sliderPies.valueProperty().multiply(.3048);

        TextField txtPulgadas = new TextField();
        txtPulgadas.textProperty().bind(pulgadas.asString("En pulgadas: %.2f"));

        TextField txtMetros = new TextField();
        txtMetros.textProperty().bind(metros.asString("En metros: %.2f"));
        vbox.getChildren().add(sliderPies);
        vbox.getChildren().add(txtMetros);
        vbox.getChildren().add(txtPulgadas);
        return vbox;
    }
    public FlowPane addFlowPaneMarketing() {
        FlowPane flow = new FlowPane();
        flow.setId("flow");
        flow.setPrefWrapLength(170); // preferred width allows for two columns

        ImageView[] pages = new ImageView[8];
        for (int i=0; i<6; i++) {
            String archivoRuta="graphics/chart_marketing-"+(i+1)+".png";

            ImageView imagen=new ImageView(new Image(Objects.requireNonNull(HelloApplication.class.getResourceAsStream(
                    archivoRuta))));

            pages[i] = imagen;

            pages[i].setFitHeight(80);
            pages[i].setFitWidth(80);

            flow.getChildren().add(pages[i]);
        }

        return flow;
    }
    public HBox addHBox() {
        HBox hboxTop = new HBox();
        hboxTop.setId("hboxTop");

        Button buttonCurrent = new Button("Current");
        buttonCurrent.setId("buttonCurrent");

        Button projected = new Button("Projected");
        projected.setId("projected");

        projected.setOnAction(HelloController::changeToScene2);


        hboxTop.getChildren().addAll(buttonCurrent, projected);


        return hboxTop;

    }

    public VBox addVBox() {
        VBox vboxLeft = new VBox();
        vboxLeft.setId("vboxLeft");


        Text title = new Text("Data");
        title.setId("Data");
        vboxLeft.getChildren().add(title);


        Hyperlink sales = new Hyperlink("Sales");
        sales.setOnAction(actionEvent -> {
            stageOriginal.setTitle("Ventana Sales");
            stageOriginal.setScene(scene);
            stageOriginal.show();
        });


        Hyperlink Marketing = new Hyperlink("Marketing");
        Marketing.setOnAction(actionEvent -> {
            stageOriginal.setTitle("Ventana Marketing");
            stageOriginal.setScene(sceneMarketing);
            stageOriginal.show();
        });

        Hyperlink Distribution = new Hyperlink("Distribution");
        Distribution.setOnAction(actionEvent -> {
            stageOriginal.setTitle("Ventana Distribution");
            stageOriginal.setScene(sceneDistribution);
            stageOriginal.show();
        });

        Hyperlink Costs = new Hyperlink("Costs");
        Costs.setOnAction(actionEvent -> {
            stageOriginal.setTitle("Ventana Costs");
            stageOriginal.setScene(sceneCost);
            stageOriginal.show();
        });


        Hyperlink[] options = new Hyperlink[]{
                sales,
                Marketing,
                Distribution,
                Costs};


        for (int i = 0; i < 4; i++) {
            options[i].setId("hijoOption");
            vboxLeft.getChildren().add(options[i]);
        }

        return vboxLeft;
    }

    public void addStackPane(HBox hb) {
        StackPane stack = new StackPane();
        stack.setId("stack");

        Rectangle helpIcon = new Rectangle(30.0, 25.0);
        helpIcon.setId("helpIcon");

        Text helpText = new Text("?");
        helpText.setId("helpText");
        helpText.setFill(Color.WHITE);

        stack.getChildren().addAll(helpIcon, helpText);


        StackPane.setMargin(helpText, new Insets(0, 10, 0, 0));

        hb.getChildren().add(stack);
        HBox.setHgrow(stack, Priority.ALWAYS);
    }


    public GridPane addGridPane(String texto, String porcentaje1, String porcentaje2) {
        GridPane grid = new GridPane();
        grid.setId("gridMedio");

        Text category = new Text(texto);
        category.setId("category");
        grid.add(category, 1, 0);

        Text chartTitle = new Text("Current Year");
        chartTitle.setId("chartTitle");
        grid.add(chartTitle, 2, 0);

        Text chartSubtitle = new Text("Goods and Services");
        chartSubtitle.setId("chartSubtitle");
        grid.add(chartSubtitle, 1, 1, 2, 1);


        ImageView imageHouse = new ImageView(
                new Image(Objects.requireNonNull(HelloApplication.class.getResourceAsStream("graphics/house2.png"))));
        imageHouse.setId("imageHouse");

        imageHouse.setFitHeight(50);
        imageHouse.setFitWidth(50);

        grid.add(imageHouse, 0, 0, 1, 2);


        Text goodsPercent = new Text(porcentaje1 + "\n80%");
        goodsPercent.setId("goodsPercent");
        GridPane.setValignment(goodsPercent, VPos.BOTTOM);
        grid.add(goodsPercent, 0, 2);


        ImageView imageChart = new ImageView(
                new Image(Objects.requireNonNull(HelloApplication.class.getResourceAsStream("graphics/piechart3.png"))));
        imageChart.setId("imageChart");
        imageChart.setFitWidth(250);//tamaño
        imageChart.setFitHeight(250);//tamaño
        grid.add(imageChart, 1, 2, 2, 1);


        Text servicesPercent = new Text(porcentaje2 + "\n20%");
        servicesPercent.setId("servicesPercent");
        GridPane.setValignment(servicesPercent, VPos.TOP);
        grid.add(servicesPercent, 3, 2);


        return grid;
    }

    public FlowPane addFlowPaneSales() {
        FlowPane flow = new FlowPane();
        flow.setId("flow");
        flow.setPrefWrapLength(170);

        ImageView[] pages = new ImageView[8];
        for (int i = 0; i < 8; i++) {
            String archivoRuta2 = "graphics/chart_" + (i + 1) + ".png";


            ImageView imagen = new ImageView(new Image(Objects.requireNonNull(HelloApplication.class.getResourceAsStream(
                    archivoRuta2))));


            pages[i] = imagen;


            pages[i].setFitHeight(80);
            pages[i].setFitWidth(80);


            flow.getChildren().add(pages[i]);
        }

        return flow;
    }


    public AnchorPane addAnchorPane(GridPane grid) {
        AnchorPane anchorpane = new AnchorPane();
        anchorpane.setId("anchorPane");


        Button buttonSave = new Button("Save");
        buttonSave.setId("buttonSave");

        Button buttonCancel = new Button("Cancel");
        buttonCancel.setId("buttonCancel");

        HBox hb = new HBox();
        hb.setId("HboxBottom");

        hb.getChildren().addAll(buttonSave, buttonCancel);

        anchorpane.getChildren().addAll(grid, hb);
        AnchorPane.setBottomAnchor(hb, 8.0);
        AnchorPane.setRightAnchor(hb, 5.0);
        AnchorPane.setTopAnchor(grid, 10.0);

        return anchorpane;
    }


}
